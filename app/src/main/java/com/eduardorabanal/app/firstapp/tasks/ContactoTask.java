package com.eduardorabanal.app.firstapp.tasks;

import android.os.AsyncTask;

import com.eduardorabanal.app.firstapp.activities.ContactoActivity;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by USER on 07/06/2017.
 */
public class ContactoTask extends AsyncTask<String, Void, String> {
    private ContactoActivity activity;

    public ContactoTask(ContactoActivity activity) {
        this.activity=activity;
    }

    @Override
    protected String doInBackground(String... params) {
        //0 o más parametros
        HttpURLConnection connection = null;
        String respuesta=null;
        try {
            //configurar
            URL url = new URL("http://www.mocky.io/v2/5938c4711200004505a67337");
            connection= (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            //obtener datos
            InputStream is=connection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(is));

            respuesta=r.readLine();
            r.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return respuesta;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            activity.LlenarContacto(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

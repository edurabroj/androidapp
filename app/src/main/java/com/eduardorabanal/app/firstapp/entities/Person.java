package com.eduardorabanal.app.firstapp.entities;

/**
 * Created by USER on 21/06/2017.
 */
public class Person {
    private String Id, Name, Lastname;

    public Person(String id, String name, String lastname) {
        Id = id;
        Name = name;
        Lastname = lastname;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }
}

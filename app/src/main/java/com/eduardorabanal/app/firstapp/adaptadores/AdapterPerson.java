package com.eduardorabanal.app.firstapp.adaptadores;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp.activities.ContactoActivity;
import com.eduardorabanal.app.firstapp.activities.SecondActivity;
import com.eduardorabanal.app.firstapp.entities.Contacto;
import com.eduardorabanal.app.firstapp.entities.Person;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Created by USER on 21/06/2017.
 */
public class AdapterPerson extends BaseAdapter {
    AppCompatActivity _activity;
    int _item_layout;
    List<Person> _datos;

    public AdapterPerson(
            AppCompatActivity appContext,
            int item_layout,
            List<Person> datos) {
        _activity = appContext;
        _item_layout = item_layout;
        _datos = datos;
    }

    @Override
    public int getCount() {
        return _datos.size();
    }

    @Override
    public Person getItem(int position) {
        return _datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = _activity.getLayoutInflater();
            convertView = vi.inflate(_item_layout, parent, false);
        }

        final Person obj = getItem(position);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        id.setText (obj.getId());

        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(obj.getName());

        TextView lastname = (TextView) convertView.findViewById(R.id.lastname);
        lastname.setText(obj.getLastname());

        return convertView;
    }
}
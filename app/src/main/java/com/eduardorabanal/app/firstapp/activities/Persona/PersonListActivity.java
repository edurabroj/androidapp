package com.eduardorabanal.app.firstapp.activities.Persona;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp.activities.ContactoActivity;
import com.eduardorabanal.app.firstapp.adaptadores.AdapterContacto;
import com.eduardorabanal.app.firstapp.adaptadores.AdapterPerson;
import com.eduardorabanal.app.firstapp.entities.Contacto;
import com.eduardorabanal.app.firstapp.entities.Person;
import com.eduardorabanal.app.firstapp.tasks.ContactoListTask;
import com.eduardorabanal.app.firstapp.tasks.Persona.PersonListTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PersonListActivity extends AppCompatActivity {
    ListView lv;
    Button btnCrear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);

        lv = (ListView) findViewById(R.id.lv);
        btnCrear = (Button) findViewById(R.id.btnCrear);

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),PersonCreateActivity.class));
            }
        });

        PersonListTask lt = new PersonListTask(this);
        lt.execute();


    }

    public void LlenarLista(String respuesta) throws JSONException {
        Log.i("RESPUESTA: ", respuesta);

        List<Person> datos=GetDatosFromJson(respuesta);

        AdapterPerson adaptador =
                new AdapterPerson(
                        this,
                        R.layout.persona_item,
                        datos
                );

        lv.setAdapter(adaptador);
    }

    private List<Person> GetDatosFromJson(String respuesta) throws JSONException {
        List<Person> datos = new ArrayList<>();
        JSONArray array = new JSONArray(respuesta);

        for (int i=0; i<array.length(); i++){
            JSONObject item = array.getJSONObject(i);

            datos.add(new Person(item.getString("id"),item.getString("name"),item.getString("lastname")));
        }

        return datos;
    }
}

package com.eduardorabanal.app.firstapp.tasks;

import android.os.AsyncTask;

import com.eduardorabanal.app.firstapp.activities.ListaActivity;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by USER on 06/06/2017.
 */
public class ContactoListTask extends AsyncTask<String, Void, String> {
    private ListaActivity activity;

    public ContactoListTask(ListaActivity activity) {
        this.activity=activity;
    }

    @Override
    protected String doInBackground(String... params) {
        //0 o más parametros
        HttpURLConnection connection = null;
        String respuesta=null;
        try {
            //configurar
            URL url = new URL("http://www.mocky.io/v2/5938b21b120000ad03a67310");
            connection= (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            //obtener datos
            InputStream is=connection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(is));

            respuesta=r.readLine();
            r.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return respuesta;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            activity.LlenarLista(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

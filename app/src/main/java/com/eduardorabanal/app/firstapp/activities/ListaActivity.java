package com.eduardorabanal.app.firstapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp.adaptadores.AdapterContacto;
import com.eduardorabanal.app.firstapp.entities.Contacto;
import com.eduardorabanal.app.firstapp.tasks.ContactoListTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        ContactoListTask ct = new ContactoListTask(this);
        ct.execute();



    }

    public void LlenarLista(String respuesta) throws JSONException {
        Log.i("RESPUESTA: ", respuesta);

        ListView lvContactos = (ListView) findViewById(R.id.lv_contacto);
        List<Contacto> datos=GetDatosFromJson(respuesta);

        AdapterContacto adaptador =
                new AdapterContacto(
                        this,
                        R.layout.item_lista_,
                        datos
                );

        lvContactos.setAdapter(adaptador);
    }

    private List<Contacto> GetDatosFromJson(String respuesta) throws JSONException {
        List<Contacto> datos = new ArrayList<>();
        JSONArray array = new JSONArray(respuesta);

        for (int i=0; i<array.length(); i++){
            JSONObject item = array.getJSONObject(i);

            int id = getResources().getIdentifier(item.getString("img"), "mipmap", this.getPackageName());
            Log.i("ID",String.valueOf(id));

            datos.add(new Contacto(item.getString("nombre"),item.getString("telefono"),item.getString("correo"),id));
        }

        return datos;
    }

    private List<Contacto> GetDatos() {
        List<Contacto> datos=new ArrayList<>();
        datos.add(new Contacto("Naiby Software","1234","ns@gmail.com",R.mipmap.a));
        datos.add(new Contacto("Firu Man","4567","ns@gmail.com",R.mipmap.b));
        datos.add(new Contacto("Marrufiàn","7412","ns@gmail.com",R.mipmap.c));
        datos.add(new Contacto("Eraul","1593","ns@gmail.com",R.mipmap.d));
        datos.add(new Contacto("Javinto","2356","ns@gmail.com",R.mipmap.e));
        datos.add(new Contacto("Pythavo","0145","ns@gmail.com",R.mipmap.e));
        return datos;
    }


}

package com.eduardorabanal.app.firstapp.tasks.Persona;

import android.os.AsyncTask;
import android.util.Log;

import com.eduardorabanal.app.firstapp.activities.Persona.PersonCreateActivity;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by USER on 21/06/2017.
 */
public class PersonCreateTask extends AsyncTask<String, Void, String> {
    private PersonCreateActivity activity;
    private String et_name, et_lastname;

    public PersonCreateTask(
            PersonCreateActivity activity,
            String et_name,
            String et_lastname
    ) {
        this.activity=activity;
        this.et_name=et_name;
        this.et_lastname=et_lastname;
    }

    @Override
    protected String doInBackground(String... params) {
        //0 o más parametros
        HttpURLConnection connection = null;
        String message =null;
        try {
            //configurar
            URL url = new URL("http://10.252.94.1:8000/persons");
            connection= (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");

            connection.setDoInput(true);
            connection.setDoOutput(true);

            //obtener datos
            OutputStream s=connection.getOutputStream();
            BufferedWriter b = new BufferedWriter(new OutputStreamWriter(s,"UTF-8"));


            String datos = "name="+et_name+"&lastname="+et_lastname;
            Log.i("DATOS", datos);
            b.write(datos);
            b.flush();
            b.close();
            s.close();

            int code = connection.getResponseCode();
            message = connection.getResponseMessage();

            Log.i("RESPONDE_CODE", String.valueOf(code));
            Log.i("RESPONDE_MESSAGE", message);

            connection.connect();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return message;
    }

    @Override
    protected void onPostExecute(String s) {
        activity.Responder(s);
    }
}
package com.eduardorabanal.app.firstapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.eduardorabanal.app.firstapp.R;

public class SecondActivity extends AppCompatActivity {
    EditText et;
    Button btnSetText;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        et=(EditText) findViewById(R.id.et);
        btnSetText = (Button) findViewById(R.id.btnSetText);
        tv = (TextView)findViewById(R.id.tv);

        btnSetText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv.setText(et.getText());
            }
        });
    }
    public void go2(View sender){
        //Log.i("TAG_PRUEBA","aleluya");
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }
}

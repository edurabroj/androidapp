package com.eduardorabanal.app.firstapp.entities;

import android.media.Image;

/**
 * Created by USER on 30/05/2017.
 */
public class Contacto {
    private String Nombre;
    private String Telefono;
    private String Correo;
    private int Img;

    public int getImg() {
        return Img;
    }

    public void setImg(int img) {
        Img = img;
    }

    public Contacto(String nombre, String telefono, String correo, int img) {
        Telefono = telefono;
        Nombre = nombre;
        Correo = correo;
        Img = img;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }
}

package com.eduardorabanal.app.firstapp.activities.Persona;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp.tasks.Persona.PersonCreateTask;

public class PersonCreateActivity extends AppCompatActivity {
    EditText et_name;
    EditText et_lastname;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_create);

        et_name = (EditText) findViewById(R.id.et_name);
        et_lastname = (EditText) findViewById(R.id.et_lastname);
        btnSave = (Button) findViewById(R.id.btnSave);

        final PersonCreateActivity activity = this;

        final PersonCreateTask[] pct = new PersonCreateTask[1];

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pct[0] = new PersonCreateTask(activity, et_name.getText().toString(),et_lastname.getText().toString());
                pct[0].execute();
            }
        });


    }

    public void Responder(String s) {
        Toast.makeText(getApplicationContext(), s,Toast.LENGTH_LONG).show();
        startActivity(new Intent(getApplicationContext(), PersonListActivity.class));
    }
}

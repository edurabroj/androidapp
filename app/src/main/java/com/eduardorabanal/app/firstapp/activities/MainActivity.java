package com.eduardorabanal.app.firstapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eduardorabanal.app.firstapp.R;

public class MainActivity extends AppCompatActivity {
    Button btnGo;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //ejecutando el onCreate del padre
        super.onCreate(savedInstanceState);
        //la declaracion de setContentView ta en el padre
        setContentView(R.layout.activity_main);

        btnGo = (Button)findViewById(R.id.btnGo);
        txt = (TextView) findViewById(R.id.txt);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),SecondActivity.class));
            }
        });
    }

    /*public void go2(View sender){
        //Log.i("TAG_PRUEBA","aleluya");
        startActivity(new Intent(getApplicationContext(),SecondActivity.class));
    }*/
}

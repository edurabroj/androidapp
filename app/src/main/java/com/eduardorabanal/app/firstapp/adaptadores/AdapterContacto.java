package com.eduardorabanal.app.firstapp.adaptadores;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.eduardorabanal.app.firstapp.activities.ContactoActivity;
import com.eduardorabanal.app.firstapp.R;
import com.eduardorabanal.app.firstapp.activities.SecondActivity;
import com.eduardorabanal.app.firstapp.entities.Contacto;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

/**
 * Created by USER on 30/05/2017.
 */
public class AdapterContacto extends BaseAdapter {

    AppCompatActivity _activity;
    int _item_layout;
    List<Contacto> _datos;

    public AdapterContacto(
            AppCompatActivity appContext,
            int item_layout,
            List<Contacto> datos) {
        _activity = appContext;
        _item_layout = item_layout;
        _datos = datos;
    }

    @Override
    public int getCount() {
        return _datos.size();
    }

    @Override
    public Contacto getItem(int position) {
        return _datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater vi = _activity.getLayoutInflater();
            convertView = vi.inflate(_item_layout, parent, false);
        }

        final Contacto contacto = getItem(position);

        ImageView iv = (ImageView) convertView.findViewById(R.id.imageView);
        iv.setImageResource (contacto.getImg());

        TextView tv_nombre = (TextView) convertView.findViewById(R.id.tv_nombre);
        tv_nombre.setText(contacto.getNombre());

        TextView tv_telf = (TextView) convertView.findViewById(R.id.tv_telf);
        tv_telf.setText(contacto.getTelefono());

        TextView tv_email = (TextView) convertView.findViewById(R.id.tv_email);
        tv_email.setText(contacto.getCorreo());

        Button btnCall = (Button) convertView.findViewById(R.id.btnCall);
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(_activity.getApplicationContext(),contacto.getNombre(),Toast.LENGTH_SHORT).show();
                dialContactPhone(contacto.getTelefono());
            }
        });
        Button btnEmail = (Button) convertView.findViewById(R.id.btnEmail);
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailIntent(contacto.getCorreo());
            }
        });

        Button btnVer = (Button) convertView.findViewById(R.id.btnVer);
        btnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.startActivity(new Intent(_activity.getApplicationContext(),ContactoActivity.class));
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.startActivity(new Intent(_activity.getApplicationContext(),SecondActivity.class));
            }
        });

        return convertView;
    }

    private void dialContactPhone(final String phoneNumber) {
        if (ActivityCompat.checkSelfPermission(_activity.getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(_activity,
                    new String[]{Manifest.permission.CALL_PHONE},
                    0);

            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        _activity.startActivity(new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phoneNumber, null)));
    }
    private void emailIntent(final String email){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
        _activity.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }
    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }
}

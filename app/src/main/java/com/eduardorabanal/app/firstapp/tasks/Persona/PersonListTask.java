package com.eduardorabanal.app.firstapp.tasks.Persona;

import android.os.AsyncTask;

import com.eduardorabanal.app.firstapp.activities.ListaActivity;
import com.eduardorabanal.app.firstapp.activities.Persona.PersonListActivity;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by USER on 21/06/2017.
 */
public class PersonListTask extends AsyncTask<String, Void, String> {
    private PersonListActivity activity;

    public PersonListTask(PersonListActivity activity) {
        this.activity=activity;
    }

    @Override
    protected String doInBackground(String... params) {
        //0 o más parametros
        HttpURLConnection connection = null;
        String respuesta=null;
        try {
            //configurar
            URL url = new URL("http://10.252.94.1:8000/persons");
            connection= (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            //obtener datos
            InputStream is=connection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(is));

            respuesta=r.readLine();
            r.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return respuesta;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
            activity.LlenarLista(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}